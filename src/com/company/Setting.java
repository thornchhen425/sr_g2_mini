package com.company;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

class Setting{
    public int id;
    public boolean autoSave;
    public int setRow;

    public Setting() {
        autoSave = true;
        setRow = 5;
    }

    @Override
    public String toString() {
        Table table = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        table.setColumnWidth(0, 30, 30);
        table.addCell("Setting", new CellStyle(CellStyle.HorizontalAlign.center));
        table.addCell("Auto Save: " + autoSave);
        table.addCell("Number of Rows: " + setRow);

        return table.render();
    }
    public static Setting fromResultSet(ResultSet resultSet) throws SQLException {
        Setting setting = new Setting();
        setting.id = resultSet.getInt("id");
        setting.autoSave = resultSet.getBoolean("auto_save");
        setting.setRow = resultSet.getInt("set_row");
        return setting;
    }
}
