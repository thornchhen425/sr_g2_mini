package com.company;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Repository {
    public static ResultSet getList(String table) throws SQLException {
        String query = "SELECT * FROM " + table;
        Connection connection = DBconnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        connection.close();
        return resultSet;
    }

    public static void saveOrUpdate(String query) throws SQLException {
        Connection connection = DBconnection.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
        connection.close();
    }

    public static void dropAllRow(String table) throws SQLException {
        String query = "DELETE FROM " + table;
        Connection connection = DBconnection.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
        connection.close();
    }

    public static void backupTable(String table) throws SQLException {
        String pattern = "dd_MM_yy_hh_mm_ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        String bak = "bak_" + table + "_" + date;
        String query = String.format("""
                CREATE TABLE %s AS (SELECT * FROM %s);
                """, bak, table);
        Connection connection = DBconnection.getConnection();
        Statement statement = connection.createStatement();
        statement.executeUpdate(query);
        connection.close();

    }

    public static List<String> getAllBackupTableName() throws SQLException {
        String query = """
                SELECT TABLE_NAME                
                FROM INFORMATION_SCHEMA.TABLES              
                WHERE "table_name" ILIKE 'bak_%' AND TABLE_CATALOG='postgres'
                """;
        Connection connection = DBconnection.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        connection.close();
        List<String> tableNames = new ArrayList<>();
        while (resultSet.next()){
            tableNames.add(resultSet.getString("table_name"));
        }
        return tableNames;
    }
}
