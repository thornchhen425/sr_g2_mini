package com.company;

interface UpdateProductCommand {
    boolean execute(Product product);
}
