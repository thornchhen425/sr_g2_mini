package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconnection {
    private static final String url = "jdbc:postgresql://localhost:5432/postgres?user=postgres&password=ditway1122";
    public static Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(url);
        return connection;
    }
}
