package com.company;

public class Main {

    public static void main(String[] args) throws Exception {
        // Load postgresql driver
        Class.forName("org.postgresql.Driver");

        // Create table in database
        Helper.migration();

        // Start Application
        StockDao stock = new Stock();
        stock.show();
    }
}
