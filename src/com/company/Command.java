package com.company;

interface Command {
    boolean execute(String cmd) throws Exception;
}
